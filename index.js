// console.log("Hello World")

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let list = json.map(todos => {
		return todos.title;
	})
	console.log(list);
})

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => {
	const item = Object.values(json);
	console.log("The item " + item[2] + " on the list has a status of " + item[3]);
})

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title:"Updated To Do List Item",
		description:"To update my to do list with a diferent data structure",
		userID: 1,
		status: "Pending",
		dateCompleted: "Pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status:"Complete",
		dateCompleted:"07/09/21"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})